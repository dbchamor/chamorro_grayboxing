I attempted to recreate a very popular Call of Duty: Black Ops 2 Raid map. 
I wanted to create the area by the that is by the entrance to one of the mansions. 
It is a popular area in the map for supply drops and other perks and score streaks to be used. 

This area of the map has a pretty simple layout with a lot of basic shapes strethched, disorted, put at different angles. 
The original map is based in Hollywood Hills, Los Angeles, California with a suburban terrain.
This in game map offers a lot of distance but also plenty of possibilities for close combat. 

It is one of my favorite Black Op 2 maps and has been remastered for CoD mobile and Black Ops 3.